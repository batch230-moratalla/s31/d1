// to start our node application

// Use the "require" directive to load Node.js modules
// The "http" module let Node.js transfer data using Hyper Text Transfer Protocol a.k.a HTTP
	// It can create an HTTP server that listen to server ports such as. ..
		// 3000, 4000, 5000 (Usually used for web development)
// The "http module" is a set of individual files that containe code to create a "component" that helps establishing data transfer between application

const http = require("http"); // module is the a form of object

let port = 4000;

const server = http.createServer((request, response) => {

	if(request.url == '/greeting'){ // property of the request object contains the address's endpoint
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Hello World!');
	}
	else if (request.url == '/homepage'){
		// 200 is set by developer number for responses / 200 means successfull to access the page
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('This is the homepage');
	}
	else{
		// Set a status code for response - a 404 means not found
		response.writeHead(404, {'Content-Type' : 'text/plain'});
		response.end('404 Page not available');
	}

})

server.listen(port)

// localhost:4000/greeting - it is the property of the url that contains greeting
// Site can be reach - might be cause by its not yet nodemon is not yet running, second is to check the listening

console.log('Server now accessible at localhost ' + port);

/*
/ Mini-Activity
    // Accessing the 'homepage' route returns a message of 'This is the homepage'
/homepage
*/

