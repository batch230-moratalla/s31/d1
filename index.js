// Node,js

// Client-server architecture
/*
	At this point we know how to :
	-Develop a static front end application - it will not change, like the portfolio, it cannot be change by any user exept by the developer
	- Create and manipulate a MongoDB database
	- What if we need our front-end app that allows user that manipulate content dynamic such as adding a comment in an ordered product updating a comment in facebook deleting a profile info etc
	- if a user or an admin can change the information it will now be a dynamic website ex facebook
	- Client server architecture 
	- This time we will the one who will create a server / creating server thru our JS
*/
// benefits 
/*
	- Centralized data makes applications more scalable(able to change in scale) and maintanable
	- Multiple client apps may all use dynamically generated data
	- Workload is concentrated on the server, making clients apps lightweight
	- Improves data availablility
*/
// for js developers we have Node.js for building server-side application
/*
	What is node js
	- An open source javascriipt runtime environment for creating a server side applications
*/

// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// The "http" module let Node.js transfer data using Hyper Text Transfer Protocol a.k.a HTTP
	// It can create an HTTP server that listen to server ports such as. ..
		// 3000, 4000, 5000 (Usually used for web development)
// The "http module" is a set of individual files that containe code to create a "component" that helps establishing data transfer between application
// Clients (the are run using devices/browser example - user input, action that the server will be received. server (nodeJS/expressJS application)) communicate by exchanging individual messages (request/response)
// Request - the message sent by the client
// Response - the message sent by the server as response (ex: page timeout / page error / error code 404 / error code 200) we access a page and we received a response
// http build-in object

// We can see our code on the terminal = gitbash


let http = require("http"); //- it may slowdown the application as there are many loading kaya natin xa tinatawag

// Using the module's createServer(), we can create an HTTP server that listens to request on a specified port and gives responses back to the client
// createServer() is a method of the http object responsible for creating a server using Node.js

http.createServer(function (request, response){
	// Use the writeHead() method to:
	// Set a status code for the response - a 200 means OK
	// Set the content - type of the response - as a plain text message
	// dito natin sinesett ung status code , dito rin siniset ung response
	response.writeHead(200, {'Content-Type': 'text/plain'})

	// Send the response with the content "Hello World"
	// ung makukuha nating response pag inaccess natin ung specific port which is the 3000, 4000, 5000
	response.end("Hello World")

}).listen(4000);

console.log('Server is running at localhost: 4000');
console.log("Hello World")